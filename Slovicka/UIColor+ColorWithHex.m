//
//  UIColor+ColorWithHex.m
//  Slovicka
//
//  Created by Tomas Kraina on 03.07.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIColor+ColorWithHex.h"


@implementation UIColor (ColorWithHex)

+ (UIColor *)colorWithHex:(NSString *)hexValue {
	NSCharacterSet *trimCharSet = [NSCharacterSet characterSetWithCharactersInString:@" #"];
	NSString *trimmedString = [hexValue stringByTrimmingCharactersInSet:trimCharSet];
	
	unsigned value;
	NSScanner *hexScanner = [NSScanner scannerWithString:trimmedString];
	[hexScanner scanHexInt:&value];
	
	unsigned red = (value & 0xff0000) >> 16;
	unsigned green = (value & 0x00ff00) >> 8;
	unsigned blue = value & 0x0000ff;
	
	UIColor *color = [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1];
	
	return color;				
}

@end
