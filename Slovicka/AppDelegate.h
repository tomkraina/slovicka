//
//  AppDelegate.h
//  Slovicka
//
//  Created by Tomas Kraina on 30.06.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PagingViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) PagingViewController *pagingViewController;

@end
