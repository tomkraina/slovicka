//
//  PagingViewController.m
//  Slovicka
//
//  Created by Tomas Kraina on 30.06.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define NIB_NAME @"CardViewController"

#import "CardViewController.h"
#import "PagingViewController.h"
#import "UIColor+ColorWithHex.h"

@implementation CardViewController

@synthesize cardNumber = _cardNumber;
@synthesize item = _item;
@synthesize textLabel = _textLabel;
@synthesize textField = _textField;
@synthesize delegate = _delegate;
@synthesize deleteButton = _deleteButton;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (id)initEmptyWithCardNumber:(int)cardNumber flipped:(BOOL)flipped
{
    if (self = [super initWithNibName:NIB_NAME bundle:nil])
    {
        _cardNumber = cardNumber;
        _flipped = flipped;
        _flipBackAutomatically = NO;
        self.item = nil;
    }
    return self;
}

- (id)initWithItem:(NSDictionary *)dictionaryItem cardNumber:(int)cardNumber flipped:(BOOL)flipped
{
    if (self = [super initWithNibName:NIB_NAME bundle:nil])
    {
        _cardNumber = cardNumber;
        _flipped = flipped;
        _flipBackAutomatically = NO;
        self.item = dictionaryItem;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    if (_item) {
        [self setFlipped:_flipped];
    }
    
    UIColor *backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"card_background"]];
    self.view.backgroundColor = backgroundColor;
    
    // don't allow deleting an empty card
    if (_item == nil) {
        _deleteButton.enabled = NO;
        _deleteButton.hidden = YES;
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];

    self.item = nil;
    self.textLabel = nil;
    self.textField = nil;
}

- (void)cardViewController:(CardViewController *)controller DidAppearInScrollView:(UIScrollView *)scrollView;
{
    if (self.item == nil) {
        // the card is empty, thus show the edit UI
        [self editButtonTapped:self];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight
            || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

#pragma mark - Flip card

- (BOOL)flipped
{
    return _flipped;
}

- (void)setFlipped:(BOOL)flipped
{
    NSString *text = [_item objectForKey:(!flipped ? @"czechKey" : @"englishKey")];
    _textLabel.text = text;
    _flipped = flipped;
    if (_textField != nil)
    {
        _textField.text = text;
    }
}

- (void)flip:(BOOL)animated
{
    if (animated)
    {
        UIViewAnimationOptions options = _flipped ? UIViewAnimationOptionTransitionFlipFromBottom : UIViewAnimationOptionTransitionFlipFromTop;
        UIView *view = self.view;
        [UIView transitionWithView:view
                        duration:1.0
                           options:options
                        animations:^{
                            [self setFlipped:!_flipped];
                        } completion:NULL];
    }
    else
    {
        [self setFlipped:!_flipped];
    }
}

- (IBAction)flipButtonTapped:(id)sender
{
    [self flip:YES];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cardViewController:itemFlipped:)]) {
        [self.delegate cardViewController:self itemFlipped:_cardNumber];
    }
}

#pragma mark - edit card

- (IBAction)editButtonTapped:(id)sender
{    
    // prepare TextField
    UITextField *textField = [[UITextField alloc] initWithFrame:_textLabel.frame];
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textField.textAlignment = UITextAlignmentCenter;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.font = [UIFont fontWithName:@"Noteworthy" size:42];
    textField.textColor = [UIColor colorWithHex:@"324FA5"];
    textField.placeholder = !_flipped ? NSLocalizedString(@"czech", @"placeholder czech") : NSLocalizedString(@"english", @"placeholder english");
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.clearButtonMode = UITextFieldViewModeNever;
    textField.delegate = self;
    
    self.textField = textField;
    
    // temporarily disable scrolling in parent view
    PagingViewController *parent = (PagingViewController *)self.parentViewController;
    parent.scrollView.scrollEnabled = NO;
    
    // slide the underlying view up to center it
    [UIView transitionWithView:self.view duration:.5 options:UIViewAnimationOptionLayoutSubviews animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y -= (frame.size.height * 1/4);
            self.view.frame = frame;
    } completion:^(BOOL finishied){
        // swap views
        _textField.text = _textLabel.text;
        [_textLabel removeFromSuperview];
        [self.view addSubview:textField];
        [textField becomeFirstResponder];
    }];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (_item != nil || textField.text.length > 0)
    {
        // save the edited word
        if (_item == nil) {
            self.item = [[NSMutableDictionary alloc] init];
            // enable the delete button
            _deleteButton.enabled = YES;
            _deleteButton.hidden = NO;
        }
        [_item setValue:textField.text forKey:(!_flipped ? @"czechKey" : @"englishKey")];
        
        // notify the delegate
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(cardViewController:itemChanged:)]) {
            [self.delegate cardViewController:self itemChanged:_item];
        }
    }
    
    if (_item.count == 1)
    {
        // flip to fill the opposite side
        [self flip:YES];
        _textField.placeholder = !_flipped ? NSLocalizedString(@"czech", @"placeholder czech") : NSLocalizedString(@"english", @"placeholder english");
        _flipBackAutomatically = YES;
        return NO;
    }
    else
    {
        // flip back after filling both sides of a new card
        if (_flipBackAutomatically)
        {
            [self flip:YES];
            _flipBackAutomatically = NO;
        }
        
        // swap views
        [_textField resignFirstResponder];
        _textLabel.text = _textField.text;
        [_textField removeFromSuperview];
        [self.view addSubview:_textLabel];
        
        // hide the textfield and show label, slide the underlying view back down
        [UIView transitionWithView:self.view duration:.5 options:UIViewAnimationOptionLayoutSubviews animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y += (frame.size.height * 1/4);
            self.view.frame = frame;
        } completion:^(BOOL finishied){
            // enable scrolling in parent view again
            PagingViewController *parent = (PagingViewController *)self.parentViewController;
            parent.scrollView.scrollEnabled = YES;
        }];
        
        return YES;
    }
}

#pragma mark - delete card

- (IBAction)deleteButtonTapped:(id)sender
{
    UIActionSheet *confirmationDialog = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Delete card?", @"Delete card question") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Delete card cancel button") destructiveButtonTitle:NSLocalizedString(@"Delete", @"Delete card delete button") otherButtonTitles:nil];
    
    [confirmationDialog showInView:self.view];
}

#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    // delete card
    [UIView transitionWithView:self.view duration:1 options:UIViewAnimationOptionLayoutSubviews animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y += self.view.bounds.size.height;
        self.view.frame = frame;
    } completion:^(BOOL finished){
        // notify the delegate
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(cardViewController:itemDeleted:)]) {
            [self.delegate cardViewController:self itemDeleted:_cardNumber];
        }
    }];
}


@end
