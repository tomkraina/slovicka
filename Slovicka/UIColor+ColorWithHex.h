//
//  UIColor+ColorWithHex.m
//  Slovicka
//
//  Created by Tomas Kraina on 03.07.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIColor (ColorWithHex)

+ (UIColor *)colorWithHex:(NSString *)hexValue;

@end
