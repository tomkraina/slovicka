//
//  PagingViewController.h
//  Slovicka
//
//  Created by Tomas Kraina on 30.06.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CardViewController;

@protocol CardViewControllerDelegate <NSObject>
- (void)cardViewController:(CardViewController *)controller itemFlipped:(int)cardNumber;;
- (void)cardViewController:(CardViewController *)controller itemChanged:(NSDictionary *)item;
- (void)cardViewController:(CardViewController *)controller itemDeleted:(int)cardNumber;
@end

@interface PagingViewController : UIViewController <UIScrollViewDelegate, CardViewControllerDelegate> {    
    UIScrollView __weak *_scrollView;
    NSMutableArray *_cardViewControllers;
    NSMutableArray *_dictionaryArray;
    int wordCount;
    
    BOOL _flipped;
}

@property(weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property(strong, nonatomic) NSMutableArray *cardViewControllers;
@property(strong, nonatomic) NSMutableArray *dictionaryArray;
@property(nonatomic) int wordCount;

@end
