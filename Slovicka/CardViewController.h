//
//  PagingViewController.h
//  Slovicka
//
//  Created by Tomas Kraina on 30.06.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PagingViewController.h"

@protocol PagingViewControllerDelegate
- (void)cardViewController:(CardViewController *)controller DidAppearInScrollView:(UIScrollView *)scrollView;
@end

@interface CardViewController : UIViewController <UITextFieldDelegate, UIActionSheetDelegate, PagingViewControllerDelegate> {
    NSDictionary *_item;
    UILabel *_textLabel;
    UITextField *_textField;
    BOOL _flipped;
    BOOL _flipBackAutomatically;
    int _cardNumber;
    id<CardViewControllerDelegate> __weak _delegate;
}

@property(nonatomic) int cardNumber;
@property(strong, nonatomic) NSDictionary *item;
@property(strong, nonatomic) IBOutlet UILabel *textLabel;
@property(strong, nonatomic) UITextField *textField;
@property(weak, nonatomic) IBOutlet UIButton *deleteButton;
@property(nonatomic) BOOL flipped;
@property(weak, nonatomic) id<CardViewControllerDelegate> delegate;

- (id)initEmptyWithCardNumber:(int)cardNumber flipped:(BOOL)flipped;
- (id)initWithItem:(NSDictionary *)dictionaryItem cardNumber:(int)cardNumber flipped:(BOOL)flipped;

- (IBAction)flipButtonTapped:(id)sender;
- (IBAction)editButtonTapped:(id)sender;
- (IBAction)deleteButtonTapped:(id)sender;
- (void)flip:(BOOL)animated;

@end
