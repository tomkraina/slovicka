//
//  PagingViewController.m
//  Slovicka
//
//  Created by Tomas Kraina on 30.06.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PagingViewController.h"
#import "CardViewController.h"

@interface PagingViewController(PrivateMethods)
- (void)loadCardNumber:(int)pageNumber;
- (NSString *)dictionaryFile;
- (void)saveDictionary;
@end

@implementation PagingViewController

@synthesize scrollView = _scrollView;
@synthesize cardViewControllers = _cardViewControllers;
@synthesize dictionaryArray = _dictionaryArray;
@synthesize wordCount = _wordCount;

- (NSString *)sampleDictionaryFile
{
    return [[NSBundle mainBundle] pathForResource:@"sample_dictionary" ofType:@"plist"];
}

- (NSString *)dictionaryFile {
    static NSString *path = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        if (paths.count >= 1) {
            path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"user_dictionary.plist"];
        }
    });
    return path;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        NSString *dictionaryFile = [self dictionaryFile];
        NSMutableArray *dict = [NSMutableArray arrayWithContentsOfFile:dictionaryFile];
        if (dict == nil) {
            dictionaryFile = [self sampleDictionaryFile];
            dict = [NSMutableArray arrayWithContentsOfFile:dictionaryFile];
        }
        self.dictionaryArray = dict;
        _wordCount = [dict count];
        _flipped = NO;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
    CGFloat pageWidth = _scrollView.bounds.size.width;
    int pageNumber = floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    for (CardViewController *controller in self.childViewControllers) {
        if (controller.cardNumber < (pageNumber - 1) || controller.cardNumber > (pageNumber + 1)) {
            [controller.view removeFromSuperview];
            [_cardViewControllers replaceObjectAtIndex:controller.cardNumber withObject:[NSNull null]];
            [controller removeFromParentViewController];
        }
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // create placeholders
    // capacity is number of words plus one for empty card in the end
    NSMutableArray *placeholders = [[NSMutableArray alloc] initWithCapacity:_wordCount + 1];
    for (int i = 0; i <= _wordCount; i++)
    {
        [placeholders addObject:[NSNull null]];
    }
    self.cardViewControllers = placeholders;
    
    // configure scroll view
    CGSize contentSize = CGSizeMake(_scrollView.frame.size.width * (_wordCount + 1), _scrollView.frame.size.height);
    _scrollView.contentSize = contentSize;
    
    [self loadCardNumber:0];
    [self loadCardNumber:1];
}

- (void)viewDidUnload
{
    [super viewDidUnload];

    self.dictionaryArray = nil;
    self.cardViewControllers = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight
            || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.bounds.size.width;
    int pageNumber = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    // Prepare a card on the current page and the cards on both sides
    [self loadCardNumber:pageNumber];
    [self loadCardNumber:pageNumber+1];
    [self loadCardNumber:pageNumber-1];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.bounds.size.width;
    int pageNumber = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    CardViewController *controller = [self.cardViewControllers objectAtIndex:pageNumber];
    
    if ([controller respondsToSelector:@selector(cardViewController:DidAppearInScrollView:)]) {
        [controller cardViewController:controller DidAppearInScrollView:scrollView];
    }
}

- (void)loadCardNumber:(int)pageNumber
{
    if (pageNumber < 0 || pageNumber > _wordCount) {
        return;
    }
    
    // replace the placeholder if necessary
    CardViewController *controller = [_cardViewControllers objectAtIndex:pageNumber];
    if ((NSNull *)controller == [NSNull null])
    {
        if (pageNumber < _wordCount)
        {
            // this means the card is not empty
            NSDictionary *dictionaryItem = [_dictionaryArray objectAtIndex:pageNumber];
            controller = [[CardViewController alloc] initWithItem:dictionaryItem cardNumber:pageNumber flipped:_flipped];
        }
        else
        {
            controller = [[CardViewController alloc] initEmptyWithCardNumber:pageNumber flipped:_flipped];
        }
        controller.delegate = self;
        [self addChildViewController:controller];
        [_cardViewControllers replaceObjectAtIndex:pageNumber withObject:controller];
    }
    
    // add the controller's view to the scroll view
    if (controller.view.superview == nil)
    {
        // since app supports many orientations use 'bounds' instead of 'frame'.
        // 'width' and 'height' are sometimes swapped in 'frame'.
        CGRect bounds = _scrollView.bounds;
        bounds.origin.x = bounds.size.width * pageNumber;
        bounds.origin.y = 0;
        controller.view.frame = bounds;
        [_scrollView addSubview:controller.view];
    }
}

#pragma mark - CardViewControllerDelegate

- (void)cardViewController:(CardViewController *)controller itemFlipped:(int)cardNumber;
{
    // flip all the other controllers
    for (CardViewController *c in self.childViewControllers) {
        if (![c isEqual:controller])
        {
            [c flip:NO];
        }
    }
    _flipped = !_flipped;
}

- (void)cardViewController:(CardViewController *)controller itemChanged:(NSDictionary *)item
{
    if (controller.cardNumber < _wordCount) {
        [_dictionaryArray replaceObjectAtIndex:controller.cardNumber withObject:item];
    }
    else {
        // new item added
        [_dictionaryArray addObject:item];
        [_cardViewControllers addObject:[NSNull null]];

        // expand scrollView
        CGSize contentSize = _scrollView.contentSize;
        contentSize.width += _scrollView.bounds.size.width;
        _scrollView.contentSize = contentSize;
        
        // add a new empty card in the end of the scroll view
        [self loadCardNumber:++_wordCount];
    }
    
    // persistence
    [self saveDictionary];
}

- (void)cardViewController:(CardViewController *)controller itemDeleted:(int)cardNumber
{
    [_dictionaryArray removeObjectAtIndex:cardNumber];
    [_cardViewControllers removeObjectAtIndex:cardNumber];
    [controller removeFromParentViewController];
    _wordCount--;
    
    // persistence
    [self saveDictionary];
    
    // collapse scrollView
    CardViewController *theNextCard = [_cardViewControllers objectAtIndex:cardNumber];
    theNextCard.cardNumber--;
    [UIView transitionWithView:theNextCard.view duration:1 options:UIViewAnimationOptionLayoutSubviews animations:^{
        CGRect frame = theNextCard.view.frame;
        frame.origin.x -= _scrollView.bounds.size.width;
        theNextCard.view.frame = frame;
    } completion:NULL];
    
    for (CardViewController *card in self.childViewControllers) {
        if (card.cardNumber > (cardNumber+1)) {
            card.cardNumber--;
            CGRect frame = card.view.frame;
            frame.origin.y -= _scrollView.bounds.size.width;
            card.view.frame = frame;
        }
    }
    
    CGSize contentSize = _scrollView.contentSize;
    contentSize.width -= _scrollView.bounds.size.width;
    _scrollView.contentSize = contentSize;
}

- (void)saveDictionary
{
    NSString *file = [self dictionaryFile];
    BOOL success = [_dictionaryArray writeToFile:file atomically:YES];
    if (!success) {
        NSLog(@"Saving dictionary to file '%@' failed.", file);
    }
}

@end
